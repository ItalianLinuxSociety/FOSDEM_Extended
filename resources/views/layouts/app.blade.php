<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FOSDEM Extended') }}</title>

    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/layout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />

    <meta name="dcterms.creator" content="Italian Linux Society" />
    <meta name="dcterms.type" content="Text" />
    <link rel="publisher" href="http://www.ils.org/" />

    <meta name="twitter:title" content="{{ config('app.name', 'FOSDEM Extended') }}" />
    <meta name="twitter:creator" content="@ItaLinuxSociety" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:url" content="http://fosdem.linux.it/" />
    <meta name="twitter:image" content="{{ asset('immagini/tw.png') }}" />

    <meta property="og:site_name" content="{{ config('app.name', 'FOSDEM Extended') }}" />
    <meta property="og:title" content="{{ config('app.name', 'FOSDEM Extended') }}" />
    <meta property="og:url" content="http://fosdem.linux.it/" />
    <meta property="og:image" content="{{ asset('immagini/fb.png') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:country-name" content="Italy" />
    <meta property="og:email" content="webmaster@linux.it" />
    <meta property="og:locale" content="it_IT" />
    <meta property="og:description" content="Il meglio dell'opensource a casa tua" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
</head>

<body class="html front not-logged-in one-sidebar sidebar-second page-node footer-columns">
<div id="page-wrapper">
	<div id="page">
		<div id="header" class="without-secondary-menu">
			<a href="/" title="Home" rel="home" id="logo">
				<img src="{{ asset('immagini/logo.png') }}" alt="Home">
			</a>

			<div id="name-and-slogan">
				<h1 id="site-name">
					<span>{{ config('app.name', 'FOSDEM Extended') }}</span>
				</h1>

				<div id="site-slogan">
					Il meglio dell'opensource a casa tua
				</div>
			</div>

			<div class="region region-header">
				<div id="block-block-3" class="block block-block">
					<div class="content">
						<div class="social">
							<a href="https://gitlab.com/ItalianLinuxSociety/FOSDEM_Extended"><img src="{{ asset('immagini/gitlab.png') }}" alt="FOSDEM Extended su GitLab"></a>
						</div>
					</div>
				</div>
			</div>

			<div id="secondary-menu" class="navigation">
				<a href="#0" class="secondary-menu-trigger">
					<img src="//www.linux.it/sites/all/themes/linuxday2/logo.png" style="width: 100%">
				</a>

				<nav id="cd-main-nav">
					<ul id="secondary-menu-links" class="links inline clearfix">
						<li class="first"><a href="/">Home</a></li>
                        <li class="last"><a href="{{ route('howto') }}">Organizzati</a></li>
                        <li class="last"><a href="{{ route('local.index') }}">Eventi Locali</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div id="main-wrapper" class="clearfix">
			<div id="{{ isset($larger) ? 'main-large' : 'main' }}" class="clearfix">
				<div id="content">
					<div class="section">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <div id="footer-wrapper">
            <div class="section">
                <div id="footer-columns" class="clearfix">
                    <div class="region region-footer-firstcolumn">
                        <div id="block-block-7" class="block block-block">
                            <div class="content">
                                <span style="text-align: center; display: block">
                                    <a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license"><img alt="AGPLv3 License" style="border-width:0" src="{{ asset('immagini/agpl3.svg') }}"></a><br/><a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license"><img alt="Creative Commons License" style="border-width:0" src="{{ asset('immagini/cczero.png') }}"></a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="region region-footer-secondcolumn">
                        <div id="block-simplenews-2" class="block block-simplenews">
                            <h2>Resta Aggiornato!</h2>
                            <script type="text/javascript" src="https://www.linux.it/external/widgetnewsletter.js"></script>
                            <div id="widgetnewsletter"></div>
                        </div>
                    </div>
                    <div class="region region-footer-thirdcolumn">
                        <div id="block-block-10" class="block block-block">
                            <h2>Amici</h2>
                            <div class="content">
                                <p style="text-align: center">
                                    <a href="//www.ils.org/info#aderenti">
                                        <img src="//www.ils.org/sites/ils.org/files/associazioni/getrand.php" border="0">
                                        <br> Scopri tutte le associazioni che hanno aderito a ILS.
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="region region-footer-fourthcolumn">
                        <div id="block-block-8" class="block block-block">
                            <h2>Network</h2>

                            <div class="content">
                                <script type="text/javaScript" src="//www.linux.it/external/widgetils.php?referrer=linuxsi"></script>
                                <div id="widgetils"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(['disableCookies']);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//pergamena.lugbs.linux.it/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', 10]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//pergamena.lugbs.linux.it/piwik.php?idsite=10" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

</body>
</html>

<style>
#address_map {
    width: 100%;
    height: 500px;
}
</style>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFkYm9iIiwiYSI6ImNqa2JsdnJqYzB5cHQzcHBkOWI0dTZ4OWMifQ.tnQnu1GuUnt1k3QvPqwe0w';
    var map = new mapboxgl.Map({
        container: 'address_map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [12.4777, 41.8961],
        zoom: 3,
        maxBounds: [
            [0.626, 34.633],
            [25.871, 47.821]
        ]
    });

    var lng = $('input:hidden[name=lng]').val();
    var lat = $('input:hidden[name=lat]').val();
    if (lng == '' || lat == '') {
        lng = 12.4777;
        lat = 41.8961;
    }

    var marker = new mapboxgl.Marker({
        draggable: true
    }).setLngLat([lng, lat]).addTo(map);

    function onDragEnd() {
        var lngLat = marker.getLngLat();
        $('input:hidden[name=lng]').val(lngLat.lng);
        $('input:hidden[name=lat]').val(lngLat.lat);

        var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + lngLat.lng + ',' + lngLat.lat + '.json?types=address&access_token=' + mapboxgl.accessToken;

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                try {
                    var full_address = data.features[0].place_name;
                    $('input[name=address]').val(full_address.substring(0, full_address.indexOf(',')));

                    for (var i = 0; i < data.features[0].context.length; i++) {
                        if (data.features[0].context[i].id.indexOf('place') == 0)
                            $('input[name=city]').val(data.features[0].context[i].text);
                        else if (data.features[0].context[i].id.indexOf('region') == 0) {
                            console.log(data.features[0].context[i].short_code);
                            $('select[name=province] option[value=' + (data.features[0].context[i].short_code.substring(3)) + ']').prop('selected', true);
                        }
                    }
                }
                catch(error) {
                    $('input[name=address]').val('');
                    $('input[name=city]').val('');
                    $('select[name=province] option:first').prop('selected', true);
                }
            }
        });
    }

    marker.on('dragend', onDragEnd);
</script>

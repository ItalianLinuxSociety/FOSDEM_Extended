@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h2>{{ $local->city }} ({{ $local->province }})</h2>
            <h3>{{ $local->address }}</h3>

            <?php $dates = effectiveDates() ?>
            <h4>
                @if($local->has_saturday && $local->has_sunday)
                    {{ $dates->saturday->printable }} e {{ $dates->sunday->printable }}
                @elseif($local->has_saturday)
                    {{ $dates->saturday->printable }}
                @elseif($local->has_sunday)
                    {{ $dates->sunday->printable }}
                @endif
            </h4>

            @if($local->year == env('CURRENT_YEAR'))
                @if($user && $user->id == $local->owner->id)
                    <form method="POST" action="{{ route('local.destroy', $local->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Elimina evento</button>
                    </form>
                    <a class="btn btn-default" href="{{ route('local.edit', $local->id) }}">Modifica evento</a>
                @endif
            @endif

            <hr>

            <p>
                {!! htmlize($local->description) !!}
            </p>
        </div>

        <div class="col-md-6">
            <div id="address_map"></div>
        </div>
    </div>

    <br><br><br>

    <?php $devrooms = $local->devrooms ?>
    <div class="row">
        <div class="col-md-12">
            @if($devrooms->isEmpty())
                <div class="alert alert-info">
                    Questo evento locale non ha devroom selezionate: i talk da seguire saranno scelti sul momento dai partecipanti!
                </div>
            @else
                <p>
                    Gli organizzatori di questo evento locale hanno selezionato le seguenti devroom, che saranno seguite nel corso della/e giornata/e:
                </p>
                <table>
                    <tbody>
                        @foreach($devrooms as $devroom)
                            <tr>
                                <td>{{ $devroom->title }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>

<style>
#address_map {
    width: 100%;
    height: 300px;
}
</style>

<script>
    var lng = {{ $local->lng }};
    var lat = {{ $local->lat }};

    mapboxgl.accessToken = 'pk.eyJ1IjoibWFkYm9iIiwiYSI6ImNqa2JsdnJqYzB5cHQzcHBkOWI0dTZ4OWMifQ.tnQnu1GuUnt1k3QvPqwe0w';
    var map = new mapboxgl.Map({
        container: 'address_map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [lng, lat],
        zoom: 12,
        maxBounds: [
            [0.626, 34.633],
            [25.871, 47.821]
        ]
    });

    var marker = new mapboxgl.Marker().setLngLat([lng, lat]).addTo(map);
</script>
@endsection

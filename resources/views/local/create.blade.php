@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>
                Per organizzare il FOSDEM Extended occorre avere:
            </p>
            <ul>
                <li>una stanza abbastanza capiente (20/50 persone) che possa essere occupata per tutto il weekend, sabato e domenica (oppure, alla peggio, solo sabato o solo domenica), entro gli orari del FOSDEM (dalle 9:00 alle 19:00, ma dipende anche dai talk che verranno selezionati dal tuo pubblico). Può andar bene una scuola, un ufficio, o un locale pubblico (un bar, un ristorante, una birreria...). Laddove possibile, e date le opportune risorse, puoi anche allestire più di una sala per poter avere più sessioni ed accontentare molte più persone che vogliono vedere talk diversi in contemporanea!</li>
                <li>buona connettività a internet: fare streaming audio/video tutto il giorno richiede una banda stabile!</li>
                <li>un proiettore, una superficie su cui proiettare, e delle casse audio</li>
            </ul>
            <p>
                Se hai tutti questi requisiti, registra qua sotto la tua istanza locale.
            </p>
        </div>
    </div>

    <br><br><br>

    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" method="POST" action="{{ route('local.store') }}">
                @csrf

                <div class="form-group">
                    <label class="col-sm-2 control-label">Mappa</label>
                    <div class="col-sm-10">
                        <div id="address_map"></div>
                        <input type="hidden" name="lng" value="{{ old('lng') }}" required>
                        <input type="hidden" name="lat" value="{{ old('lat') }}" required>
                        <span class="help-block">Clicca e trascina il marker sulla mappa per selezionare il punto che apparirà sulla mappa.</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="province" class="col-sm-2 control-label">Provincia</label>
                    <div class="col-sm-10">
                        @include('commons.province', ['name' => 'province', 'default_value' => ''])
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">Città</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="days" class="col-sm-2 control-label">Giorni</label>
                    <div class="col-sm-10">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input type="radio" name="days" id="both" value="both" autocomplete="off" checked> Sabato e Domenica
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="days" id="saturday" value="saturday" autocomplete="off"> Solo Sabato
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="days" id="sunday" value="sunday" autocomplete="off"> Solo Domenica
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Indirizzo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Descrizione</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                        <span class="help-block">Per il pranzo, tutti mettono 5 euro e qualcuno va a prendere delle pizze? Scrivilo qui! Occorrono indicazioni specifiche per raggiungere il posto (e.g. "Citofonare Mario")? Scrivilo qui! Intendete tenere una sessione speciale per guardare tutti i talk della devroom dedicata a BSD? Scrivilo qui!</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="rooms" class="col-sm-2 control-label">Sale Disponibili</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="rooms" step="1" min="1" value="1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="rooms" class="col-sm-2 control-label">Devroom Selezionate</label>
                    <div class="col-sm-10">
                        <p>
                            Se hai degli interessi particolari e specifici, seleziona qui le devroom che intendi seguire durante il tuo FOSDEM Extended (<a href="https://fosdem.org/{{ env('CURRENT_YEAR') }}/schedule/" target="_blank">qui trovi il programma completo</a>, coi relativi talks). Se non ne selezioni nessuna, si assume che i talk da vedere insieme saranno decisi sul momento dai partecipanti.
                        </p>

                        @foreach(App\Devroom::where('year', env('CURRENT_YEAR'))->orderBy('title')->get() as $devroom)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="devroom[]" value="{{ $devroom->id }}"> {{ $devroom->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Registra evento</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('local.extraedit')

@endsection

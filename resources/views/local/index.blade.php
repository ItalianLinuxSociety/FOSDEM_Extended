@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 text-right">
            @if($user)
                @if($user->own == null)
                    <a class="btn btn-lg btn-default" href="{{ route('local.create') }}">Organizza il FOSDEM Extended nella tua città!</a>
                @else
                    <a class="btn btn-lg btn-default" href="{{ route('local.show', $user->own->id) }}">Vedi il tuo FOSDEM Extended</a>
                @endif
            @endif
        </div>
    </div>

    <br><br><br>

    <div class="row">
        @if($locals->isEmpty())
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-info">
                    <p>
                        Non sono ancora stati registrati eventi locali per quest'anno.
                    </p>
                    <p>
                        <a href="{{ route('local.create') }}">Clicca qui e fallo tu per primo!</a>
                    </p>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="30%">Luogo</th>
                            <th width="15%">Giorni</th>
                            <th width="30%">Descrizione</th>
                            <th width="25%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($locals as $local)
                            <tr>
                                <td>
                                    <strong>{{ sprintf('%s (%s)', $local->city, $local->province) }}</strong><br>{{ $local->address }}
                                </td>
                                <td>
                                    @if($local->has_saturday && $local->has_sunday)
                                        Sabato e Domenica
                                    @elseif($local->has_saturday)
                                        Solo Sabato
                                    @else
                                        Solo Domenica
                                    @endif
                                </td>
                                <td>
                                    {!! htmlize($local->description) !!}
                                </td>
                                <td>
                                    <a class="btn btn-default" href="{{ route('local.show', $local->id) }}">Vedi Dettagli</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" method="POST" action="{{ route('local.update', $local->id) }}">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label class="col-sm-2 control-label">Mappa</label>
                    <div class="col-sm-10">
                        <div id="address_map"></div>
                        <input type="hidden" name="lng" value="{{ $local->lng }}" required>
                        <input type="hidden" name="lat" value="{{ $local->lat }}" required>
                        <span class="help-block">Clicca e trascina il marker sulla mappa per selezionare il punto che apparirà sulla mappa.</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="province" class="col-sm-2 control-label">Provincia</label>
                    <div class="col-sm-10">
                        @include('commons.province', ['name' => 'province', 'default_value' => '', 'selected' => $local->province])
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">Città</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city" value="{{ $local->city }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="days" class="col-sm-2 control-label">Giorni</label>
                    <div class="col-sm-10">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary {{ $local->has_saturday && $local->has_sunday ? 'active' : '' }}">
                                <input type="radio" name="days" id="both" value="both" autocomplete="off" {{ $local->has_saturday && $local->has_sunday ? 'checked' : '' }}> Sabato e Domenica
                            </label>
                            <label class="btn btn-primary {{ $local->has_saturday && $local->has_sunday == false ? 'active' : '' }}">
                                <input type="radio" name="days" id="saturday" value="saturday" autocomplete="off" {{ $local->has_saturday && $local->has_sunday == false ? 'checked' : '' }}> Solo Sabato
                            </label>
                            <label class="btn btn-primary {{ $local->has_saturday == false && $local->has_sunday ? 'active' : '' }}">
                                <input type="radio" name="days" id="sunday" value="sunday" autocomplete="off" {{ $local->has_saturday == false && $local->has_sunday ? 'checked' : '' }}> Solo Domenica
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Indirizzo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" value="{{ $local->address }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Descrizione</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="description">{{ $local->description }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="rooms" class="col-sm-2 control-label">Sale Disponibili</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="rooms" step="1" min="1" value="{{ $local->rooms }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="rooms" class="col-sm-2 control-label">Devroom Selezionate</label>
                    <div class="col-sm-10">
                        @foreach(App\Devroom::where('year', $local->year)->orderBy('title')->get() as $devroom)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="devroom[]" value="{{ $devroom->id }}" {{ $local->devrooms()->where('devroom_id', $devroom->id)->count() == 0 ? '' : 'checked' }}> {{ $devroom->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Salva evento</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('local.extraedit')

@endsection

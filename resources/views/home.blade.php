@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <p>
                Ogni anno, ad inizio febbraio, circa 8000 tra sviluppatori, sistemisti ed addetti ai lavori si ritrovano a Bruxelles, in Belgio, per il <strong>Free Open Source Developers European Meeting</strong> (per gli amici: <a href="https://fosdem.org/">FOSDEM</a>), il principale convegno europeo dedicato al software libero e opensource.
            </p>
            <p>
                Non tutti riescono a partecipare, per motivi di studio o lavoro, ma vorremmo permettere a tutti di vivere almeno in parte l'"esperienza FOSDEM" pur restando nella propria città.
            </p>
            <h2>
                Quest'anno il FOSDEM è<br>
                <?php $dates = effectiveDates() ?>
                {{ $dates->saturday->printable }}<br>
                {{ $dates->sunday->printable }}<br>
            </h2>
            <br>
            <p>
                Problemi? Dubbi? Segnalazioni? Scrivi a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>!
            </p>
        </div>
        <div class="col-md-6">
            <div class="well">
                <p>
                    Organizza il <strong>FOSDEM Extended</strong> nella tua città!
                </p>
                <p>
                    Ti serve:
                </p>
                <ul>
                    <li>una stanza abbastanza capiente (20/50 persone) che possa essere occupata sabato e/o domenica. Può andar bene una scuola, un ufficio, o un locale pubblico</li>
                    <li>buona connettività a internet</li>
                    <li>un proiettore, una superficie su cui proiettare, e delle casse audio</li>
                </ul>
                <p>
                    Autenticati e registra il tuo evento locale!
                </p>
            </div>
        </div>
    </div>

    <br><br>

    <div class="row">
        <div class="col-md-12 text-center">
            @if(Auth::check())
                <a class="btn btn-default btn-lg" href="{{ route('local.index') }}">Crea ed esplora eventi locali</a>
            @else
                <div class="btn-group" role="group">
                    <a class="btn btn-default btn-lg" href="{{ route('login') }}">Autenticati su Servizi.Linux.it</a>
                    <a class="btn btn-default btn-lg" href="{{ route('login.gitlab') }}">Autenticati con GitLab</a>
                    <a class="btn btn-default btn-lg" href="{{ route('login.facebook') }}">Autenticati con Facebook</a>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

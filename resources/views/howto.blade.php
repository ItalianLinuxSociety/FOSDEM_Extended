@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>Per i LUG</h4>
            <p>
                Il FOSDEM Extended offre una opportunità per allestire - con relativamente poco sforzo - un evento di interesse per i più giovani e per approfondire tematiche comuni all'interno della community freesoftware.
            </p>
            <p>
                Devroom consigliate:
            </p>
            <ul>
                <li>Community devroom</li>
                <li>Decentralized Internet and Privacy</li>
                <li>Distributions</li>
                <li>Legal and Policy Issues</li>
                <li>Security</li>
            </ul>

            <hr>

            <h4>Per le aziende</h4>
            <p>
                Se nel tuo ufficio c'è una sala riunioni abbastanza spaziosa, questa è l'occasione per richiamare presso di te giovani talenti locali con interesse nel tuo specifico settore tecnologico.
            </p>
            <p>
                Devroom consigliate:
            </p>
            <ul>
                <li>Continuous Integration and Continuous Deployment</li>
                <li>Embedded, Mobile and Automotive</li>
                <li>HPC, Big Data, and Data Science</li>
                <li>Monitoring and Observability</li>
                <li>Testing and Automation</li>
            </ul>

            <hr>

            <h4>Per i co-working</h4>
            <p>
                Offri ai tuoi visitatori una occasione di networking ed aggiornamento professionale, per scoprire e commentare insieme nuovi strumenti di lavoro.
            </p>
            <p>
                Devroom consigliate:
            </p>
            <ul>
                <li>Collaborative Information and Content Management Applications</li>
                <li>Geospatial</li>
                <li>Javascript</li>
                <li>Python</li>
                <li>Web Performance</li>
            </ul>
        </div>

        <div class="col-md-6">
            <p>
                Qualche dritta per aderire al <strong>FOSDEM Extended</strong>, sia per organizzare un proprio evento locale che per partecipare insieme ad altri.
            </p>
            <ul>
                <li>
                    I contenuti del FOSDEM sono di carattere spesso molto <strong>tecnico</strong>, e ci si può aspettare che altrettanto tecnico ed esperto sia il pubblico interessato. Per questo motivo è sufficiente trovare una sala di piccole/medie dimensioni, che possa ospitare almeno 20 persone
                </li>
                <li>
                    La <strong>dotazione minima indispensabile</strong> consiste in:
                    <ul>
                        <li>un computer</li>
                        <li>un proiettore (e la relativa superficie su cui proiettare)</li>
                        <li>buona connettività all'internet, per una degna fruizione degli streaming audio/video</li>
                        <li>delle casse audio, per permettere a tutti di sentire bene i talk</li>
                    </ul>
                </li>
                <li>
                    Potete scegliere sul momento quali talk seguire, ma nulla vieta di deciderli in anticipo e dedicare il proprio FOSDEM Extended ad <strong>un particolare soggetto</strong> e seguire interamente una (o più) delle <a href="https://fosdem.org/2020/schedule/">sessioni tematiche in programma</a>.
                </li>
                <li>
                    I talk del FOSDEM si svolgono dalle 10:00 alle 18:00, senza interruzioni. Organizzatevi per il <strong>pranzo</strong>! Potete ad esempio raccogliere i soldi in sede ed andare a prendere e/o farvi portare qualche pizza (e qualche birra, magari belga per ricreare l'atmosfera del FOSDEM a Bruxelles!)
                </li>
                <li>
                    Qualora non riusciste ad organizzarvi nel weekend del FOSDEM, potete comunque fruire dei contenuti a posteriori: le <strong>registrazioni audio/video</strong> dei talk sono solitamente pubblicate sul sito nel giro di poche settimane, potete organizzare anche in altri periodi dell'anno delle serate di visione condivisa degli interventi ritenuti più interessanti.
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection

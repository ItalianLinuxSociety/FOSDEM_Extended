<?php

return [
    'gitlab' => [
        'client_id' => env('GITLAB_KEY'),
        'client_secret' => env('GITLAB_SECRET'),
        'redirect' => Config('app.url') . '/login/gitlab/callback'
    ],
];

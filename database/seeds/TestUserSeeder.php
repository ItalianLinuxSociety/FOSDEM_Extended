<?php

use Illuminate\Database\Seeder;

use App\User;

class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->username = 'test';
        $user->name = 'test';
        $user->email = 'test@mailinator.com';
        $user->admin = false;
        $user->save();
    }
}

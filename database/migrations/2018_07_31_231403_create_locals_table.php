<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('owner_id')->unsigned();
            $table->string('province');
            $table->string('city');
            $table->string('address');
            $table->text('description');
            $table->string('year');
            $table->integer('rooms')->unsigned()->default(1);
            $table->boolean('approved')->default(false);
            $table->float('lat');
            $table->float('lng');
            $table->boolean('has_saturday')->default(false);
            $table->boolean('has_sunday')->default(false);

            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locals');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('title');
            $table->string('year');
        });

        Schema::create('devroom_local', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('devroom_id')->unsigned();
            $table->integer('local_id')->unsigned();

            $table->foreign('devroom_id')->references('id')->on('devrooms')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('local_id')->references('id')->on('locals')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devroom_local');
        Schema::dropIfExists('devrooms');
    }
}

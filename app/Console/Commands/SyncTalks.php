<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use ICal\ICal;

use App\Talk;
use App\Devroom;

class SyncTalks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:talk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggiorna l\'elenco dei talk FOSDEM per l\'anno corrente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ical = new ICal();
        $url = sprintf('https://fosdem.org/%s/schedule/ical', env('CURRENT_YEAR'));
        $ical->initUrl($url);

        $skippable_devrooms = ['Certification', 'BOFs', 'Global Diversity CFP Day', 'Keysigning'];

        foreach($ical->events() as $event) {
            $skip = false;

            foreach($skippable_devrooms as $sd) {
                if (strncmp($event->categories, $sd, strlen($sd)) == 0) {
                    $skip = true;
                    break;
                }
            }

            if ($skip) {
                continue;
            }

            $talk = Talk::where('year', env('CURRENT_YEAR'))->where('uid', $event->uid)->first();
            if ($talk == null) {
                $talk = Talk::where('year', env('CURRENT_YEAR'))->where('title', $event->summary)->first();
                if ($talk == null) {
                    $talk = new Talk();
                    $talk->uid = $event->uid;
                    $talk->year = env('CURRENT_YEAR');
                }
            }

            $talk->title = $event->summary;
            $talk->url = $event->url;
            $talk->description = html_entity_decode($event->description);
            $talk->location = $event->location;
            $talk->devroom = $event->categories;
            $talk->date = date('Y-m-d', $event->dtstart_array[2]);
            $talk->start = date('H:i', $event->dtstart_array[2]);
            $talk->end = date('H:i', $event->dtend_array[2]);
            $talk->save();

            $devroom = Devroom::where('year', env('CURRENT_YEAR'))->where('title', $event->categories)->first();
            if ($devroom == null) {
                $devroom = new Devroom();
                $devroom->title = $event->categories;
                $devroom->year = env('CURRENT_YEAR');
                $devroom->save();
            }
        }
    }
}

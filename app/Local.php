<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function devrooms()
    {
        return $this->belongsToMany('App\Devroom');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;

use Auth;

use App\User;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('Aacotroneo\Saml2\Events\Saml2LoginEvent', function (Saml2LoginEvent $event) {
            $messageId = $event->getSaml2Auth()->getLastMessageId();
            $saml_user = $event->getSaml2User();
            $attributes = $saml_user->getAttributes();

            $username = $attributes['username'][0];
            $name = $attributes['displayName'][0];
            $email = $attributes['email'][0];

            $user = User::where('username', $username)->first();
            if ($user == null) {
                $user = new User();
                $user->username = $username;
            }

            $user->name = $name;
            $user->email = $email;
            $user->save();

            Auth::loginUsingId($user->id);
        });
    }
}

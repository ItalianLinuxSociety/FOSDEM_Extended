<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Local;

class LocalController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        $locals = Local::where('year', env('CURRENT_YEAR'))->orderBy('province', 'asc')->get();
        return view('local.index', compact('locals', 'user'));
    }

    public function create(Request $request)
    {
        $user = $request->user();
        if ($user == null)
            return redirect()->route('home');

        if ($user->own != null)
            return redirect()->route('local.show', $user->own->id);

        return view('local.create');
    }

    private function readFromRequest($local, $request)
    {
        $local->lng = $request->input('lng');
        $local->lat = $request->input('lat');
        $local->province = $request->input('province');
        $local->city = ucwords($request->input('city'));
        $local->address = ucwords($request->input('address'));
        $local->description = strip_tags($request->input('description'));
        $local->rooms = $request->input('rooms');

        $days = $request->input('days');
        switch($days) {
            case 'both':
                $local->has_saturday = true;
                $local->has_sunday = true;
                break;
            case 'saturday':
                $local->has_saturday = true;
                $local->has_sunday = false;
                break;
            case 'sunday':
                $local->has_saturday = false;
                $local->has_sunday = true;
                break;
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'province' => 'required|max:2',
            'city' => 'required|max:255',
            'address' => 'required|max:255',
            'description' => 'required',
            'days' => 'required',
            'rooms' => 'required|numeric',
        ]);

        $local = new Local();
        self::readFromRequest($local, $request);
        $local->year = env('CURRENT_YEAR');
        $local->approved = false;
        $local->owner_id = $request->user()->id;
        $local->save();

        $local->devrooms()->sync($request->input('devroom'));

        return redirect()->route('local.show', $local->id);
    }

    public function show(Request $request, $id)
    {
        $user = $request->user();
        $local = Local::findOrFail($id);
        return view('local.show', compact('local', 'user'));
    }

    public function edit(Request $request, $id)
    {
        $user = $request->user();
        if ($user == null)
            return redirect()->route('home');

        $local = Local::findOrFail($id);
        if ($local->owner_id != $user->id)
            return redirect()->route('home');

        return view('local.edit', compact('local'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'province' => 'required|max:2',
            'city' => 'required|max:255',
            'address' => 'required|max:255',
            'description' => 'required',
            'days' => 'required',
            'rooms' => 'required|numeric',
        ]);

        $user = $request->user();
        if ($user == null)
            return redirect()->route('home');

        $local = Local::findOrFail($id);
        if ($local->owner_id != $user->id)
            return redirect()->route('home');

        self::readFromRequest($local, $request);
        $local->save();

        $local->devrooms()->sync($request->input('devroom'));

        return redirect()->route('local.show', $local->id);
    }

    public function destroy(Request $request, $id)
    {
        $user = $request->user();
        $local = Local::findOrFail($id);

        if ($local->owner_id != $user->id)
            return redirect()->route('home');

        $local->delete();

        return redirect()->route('local.index');
    }
}

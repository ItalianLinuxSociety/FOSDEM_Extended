<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Talk extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function getStreamingUrlAttribute()
    {
        $tokens = explode(' ', $this->location);
        return sprintf('https://live.fosdem.org/watch/%s', str_slug($tokens[0]));
    }
}

<?php

function writeDate($date)
{
    return date('d/m/Y', strtotime($date));
}

function writeHour($date)
{
    return substr($date, 0, strrpos($date, ':'));
}

function effectiveDates()
{
    $ret = (object) [
        'saturday' => (object) [
            'printable' => '???'
        ],
        'sunday' => (object) [
            'printable' => '???'
        ],
    ];

    $dates = App\Talk::select('date')->distinct()->where('year', env('CURRENT_YEAR'))->get();
    if ($dates->isEmpty()) {
        $dates = config('fosdem.enforced_dates');
    }
    else {
        $dates = $dates->map(function($item, $key) {
            return $item->date;
        })->all();
    }

    foreach($dates as $d) {
        $day = strftime('%A %d %B %Y', strtotime($d));

        $day = str_replace('Saturday', 'Sabato', $day);
        $day = str_replace('Sunday', 'Domenica', $day);
        $day = str_replace('January', 'Gennaio', $day);
        $day = str_replace('February', 'Febbraio', $day);

        if (strpos($day, 'Sabato') !== false)
            $ret->saturday->printable = $day;
        else
            $ret->sunday->printable = $day;
    }

    return $ret;
}

function htmlize($string)
{
    $string = nl2br($string);
    $string = preg_replace('%(https?|ftp)://([-A-Z0-9./_*?&;=#]+)%is', '<a href="$0" target="_blank">$0</a>', $string);
    return $string;
}

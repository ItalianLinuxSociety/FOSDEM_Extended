<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function ownLocal()
    {
        return $this->hasMany('App\Local', 'owner_id');
    }

    public function getOwnAttribute()
    {
        return $this->ownLocal()->where('year', env('CURRENT_YEAR'))->first();
    }
}

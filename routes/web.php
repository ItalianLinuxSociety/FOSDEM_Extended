<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('organizzati', 'HomeController@howto')->name('howto');

Route::get('login', function() {
    if (Auth::check())
        return redirect()->route('local.index');
    else
        Aacotroneo\Saml2\Facades\Saml2Auth::login(route('home'));
})->name('login');

Route::get('login/gitlab', function() {
    return Socialite::driver('GitLab')->setScopes('read_user')->redirect();
})->name('login.gitlab');

Route::get('login/gitlab/callback', function() {
    $user = Socialite::driver('GitLab')->user();
    $extended_username = sprintf('gitlab_%s', $user->getNickname());

    $effective = App\User::where('username', $extended_username)->first();
    if ($effective == null) {
        $effective = App\User::where('email', $user->getEmail())->first();
        if ($effective == null) {
            $effective = new App\User();
            $effective->username = $extended_username;
            $effective->name = $user->getName();
            $effective->email = $user->getEmail();
            $effective->save();
        }
    }

    Auth::loginUsingId($effective->id);

    return redirect()->route('local.index');
})->name('login.callback_gitlab');

Route::get('login/facebook', function() {
    return Socialite::driver('facebook')->redirect();
})->name('login.facebook');

Route::get('login/facebook/callback', function() {
    $user = Socialite::driver('facebook')->user();
    $extended_username = sprintf('facebook_%s', $user->getNickname());

    $effective = App\User::where('username', $extended_username)->first();
    if ($effective == null) {
        $effective = App\User::where('email', $user->getEmail())->first();
        if ($effective == null) {
            $effective = new App\User();
            $effective->username = $extended_username;
            $effective->name = $user->getName();
            $effective->email = $user->getEmail();
            $effective->save();
        }
    }

    Auth::loginUsingId($effective->id);

    return redirect()->route('local.index');
})->name('login.callback_facebook');

Route::get('local/subscribe/{id}', 'LocalController@subscribe')->name('local.subscribe');

Route::resource('local', 'LocalController');

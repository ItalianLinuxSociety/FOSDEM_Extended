# FOSDEM Extended

Piattaforma di organizzazione e coordinamento per il FOSDEM Extended, per permettere la registrazione di nuovi eventi locali, la partecipazione degli utenti, e la votazione dei talks.

Per maggiori informazioni: http://fosdem.linux.it/
